extern crate rand;
use rand::Rng;
use rand::thread_rng;

fn main() {
    let swears = vec![
        "fafufufifu".to_string(),
        "Look around you!".to_string(),
        "You can't even spel rigt.".to_string(),
        "Eat some fruits!".to_string(),
        "Touch some grass!".to_string(),
        "Natural selection has failed you...".to_string(),
        "You evolved from a very stupid ancestor.".to_string(),
        "Knock! Knock! Who's there? A brainless mate!".to_string(),
        "- \"What is your definition of average?\"\n- \"You.\"".to_string(),
        "Intelligence test: negative.".to_string(),
        "Ya look like ya have eaten three bananas in the last 16 years.".to_string(),
        "Welcome to Real Life, idiot.".to_string(),
        "Ye can't respawn in here!".to_string(),
        "\"@#*+€$+~’##y14@!!!\"\n       - Your brain".to_string(),
        "Out of memory!       - Your brain".to_string(),
        "There is no CTRL-Z in here.".to_string(),
        "Muscle Test: Negative.".to_string(),
        "When turning sideways, you turn invisible.".to_string(),
        "Muscular equivalent of a noodle.".to_string(),
        "Where are y... oh, didn't see you there.".to_string(),
        "You are the \x1b[1mworst\x1b[22m shopping companion.".to_string(),
        "Vegetables are Vegan you piece of s#!@".to_string(),
        "Eating is necessary".to_string(),
        "Worse than diarreha".to_string(),
        "STUPIDITY TEST: \x1b[31mPOSITIVE\x1b[0m".to_string(),
        "\x1b[1;31mENTERING SELF DESTRUCTION MODE.\x1b[0m".to_string(),
        "Git good!".to_string(),
        "Maybe fork yourself...".to_string(),
        "You have a sit-pack, not a six-pack!".to_string(),
        "Skill issue".to_string(),
        "Brain cells go nom-nom".to_string(),
    ];
    println!("{}", random(&swears))
}

fn random(dat: &Vec<String>) -> &String {
    let len = dat.len();
    let mut rng = thread_rng();
    let rand = rng.gen_range(0..len);
    &dat[rand]
}
