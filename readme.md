# swrr - a cli program

### What?
> Ever had that moment where you just feel like your ego is the size of an elephant and just thought:
> "Letting myself get victimised by a computer writing colored text to a terminal is the perfect solution."

No? Me neither.

So here you go, `swrr` a program that spits ~~more or less~~ creative insults in your face.
